const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.get("/", (res, req) => {
    res.sendFile(path.join(__dirname, 'build', index.html));
})

app.listen(3001, function() {
    console.log("Listening to port 3001");
})
